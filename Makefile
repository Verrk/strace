#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/01 16:40:03 by cpestour          #+#    #+#              #
#    Updated: 2016/02/08 12:54:31 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=ft_strace
CC=gcc
CFLAGS=-Iincludes
FILES=main.c strace.c ft_strsplit.c ft_memalloc.c ft_strnew.c echap.c
SRC=$(addprefix srcs/, $(FILES))
OBJ=$(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $@ $^

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all
