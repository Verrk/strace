/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strace.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:40:12 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/15 10:57:10 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRACE_H
# define STRACE_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <sys/types.h>
# include <sys/ptrace.h>
# include <sys/user.h>
# include <sys/reg.h>
# include <sys/wait.h>
# include <sys/syscall.h>
# include <errno.h>

# ifdef __amd64__
#  define eax rax
#  define orig_eax orig_rax
#  define ebx rdi
#  define ecx rsi
#  define edx rdx
#  define esi r10
#  define edi r8
#  define ebp r9
# endif

# define offsetof(a, b) __builtin_offsetof(a, b)
# define get_reg(child, name) __get_reg(child, offsetof(struct user, regs.name))

int do_child(int ac, char **av);
void do_trace(pid_t child);
char **ft_strsplit(char const *s, char c);
void ft_escape_chars(char **str, long *size, int *esc);

#endif
