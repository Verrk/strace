/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 15:21:31 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/01 20:52:16 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

char		*ft_strnew(size_t size)
{
	void	*new;

	if (size <= 0)
		return (NULL);
	new = malloc(sizeof(char) * (size + 1));
	bzero(new, size + 1);
	return ((char *)new);
}
