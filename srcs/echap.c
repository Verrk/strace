/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echap.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 11:38:52 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/15 10:58:42 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

static char *convert_escape(char c)
{
	if (c == '\a')
		return ("\\a");
	else if (c == '\b')
		return ("\\b");
	else if (c == 0)
		return ("\\0");
	else if (c == 1)
		return ("\\1");
	else if (c == 2)
		return ("\\2");
	else if (c == 3)
		return ("\\3");
	else if (c == 4)
		return ("\\4");
	else if (c == 5)
		return ("\\5");
	else if (c == 6)
		return ("\\6");
	else if (c == 7)
		return ("\\7");
	else if (c == 8)
		return ("\\8");
	else if (c == '\f')
		return ("\\f");
	else if (c == '\n')
		return ("\\n");
	else if (c == '\r')
		return ("\\r");
	else if (c == '\t')
		return ("\\t");
	else if (c == '\v')
		return ("\\v");
	else if (c == '\\')
		return ("\\\\");
	else if (c == '\'')
		return ("\\'");
	else if (c == '\"')
		return ("\\\"");
	else if (c == '\?')
		return ("\\?");
	return ("  ");
}

static int is_escape(char c)
{
	return (c == '\a' ||
			c == '\b' ||
			c == '\f' ||
			c == '\n' ||
			c == '\r' ||
			c == '\t' ||
			c == '\v' ||
			c == '\\' ||
			c == '\'' ||
			c == '\"' ||
			c == '\?');
}

void ft_escape_chars(char **str, long *size, int *esc)
{
	char *dup;
	char *dup_ptr;
	const char *ptr;
	long old_size;
	long new_size;

	old_size = (*size);
	ptr = *str;
	new_size = old_size;
	while ((*size)--)
	{
		if (is_escape(*ptr++))
		{
			new_size++;
			(*esc)++;
		}
	}
	if (!(dup = (char *)malloc(sizeof(char) * (new_size + 1))))
		return ;
	*size = new_size;
	dup_ptr = dup;
	ptr = *str;
	while (old_size--)
	{
		if (is_escape(*ptr))
		{
			memcpy(dup_ptr, convert_escape(*ptr), 2);
			dup_ptr += 2;
		}
		else
			*dup_ptr++ = *ptr;
		ptr++;
	}
	*dup_ptr = '\0';
	free(*str);
	*str = dup;
}
