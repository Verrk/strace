/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:39:40 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/01 16:39:41 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strace.h>

int main(int ac, char **av)
{
	pid_t pid;

	if (ac < 2)
	{
		dprintf(2, "Usage: %s prog args\n", av[0]);
		exit(1);
	}
	pid = fork();
	if (pid == 0)
		return do_child(ac -1, av + 1);
	else
	{
		do_trace(pid);
		return 0;
	}
}
