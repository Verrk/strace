/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strace.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:42:43 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/15 11:03:21 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strace.h>
#include <syscallent.h>

long __get_reg(pid_t child, int off)
{
	long val;

	val = ptrace(PTRACE_PEEKUSER, child, off);
	return val;
}

void quit_signal(char *sig, pid_t child)
{
	kill(child, 9);
	dprintf(2, "+++ Killed by %s +++\n", sig);
	exit(0);
}

int wait_for_syscall(pid_t child)
{
	int status;

	while (42)
	{
		ptrace(PTRACE_SYSCALL, child, 0, 0);
		waitpid(child, &status, 0);
		if (status == 383)
			quit_signal("SIGHUP", child);
		else if (status == 639)
			quit_signal("SIGINT", child);
		else if (status == 895)
			quit_signal("SIGQUIT", child);
		else if (status == 895)
			quit_signal("SIGILL", child);
		else if (status == 1151)
			quit_signal("SIGABRT", child);
		else if (status == 1663)
			quit_signal("SIGFPE", child);
		else if (status == 2175 || status == 9)
			quit_signal("SIGKILL", child);
		else if (status == 2943)
			quit_signal("SIGSEGV", child);
		else if (status == 3455)
			quit_signal("SIGPIPE", child);
		else if (status == 4711)
			quit_signal("SIGALRM", child);
		else if (status == 3967)
			quit_signal("SIGTERM", child);
		else if (status == 1919)
			quit_signal("SIGBUS", child);
		else if (status == 7039)
			quit_signal("SIGPROF", child);
		else if (status == 8063)
			quit_signal("SIGSYS", child);
		else if (status == 6015)
			quit_signal("SIGURG", child);
		else if (status == 6783)
			quit_signal("SIGVTALRM", child);
		else if (status == 6271)
			quit_signal("SIGXCPU", child);
		else if (status == 6527)
			quit_signal("SIGXFSZ", child);
		else if (status == 4223)
			quit_signal("SIGSTKFLT", child);
		else if (status == 7551)
			quit_signal("SIGIO", child);
		else if (status == 7807)
			quit_signal("SIGPWR", child);
		if (WIFSTOPPED(status) && WSTOPSIG(status) & 0x80)
			return (0);
		if (WIFEXITED(status))
			return (1);
	}
}

int do_child(int ac, char **av)
{
	char *args[ac + 1];
	memcpy(args, av, ac * sizeof(char*));
	args[ac] = NULL;
	ptrace(0);
	kill(getpid(), SIGSTOP);
	return execvp(args[0], args);
}

char *read_string(pid_t child, unsigned long addr)
{
	char *val = malloc(4096);
	int allocated = 4096, read = 0;
	unsigned long tmp = 0;

	while (42)
	{
		if (read + sizeof(tmp) > (unsigned int)allocated)
		{
			allocated *= 2;
			val = realloc(val, allocated);
		}
		tmp = ptrace(PTRACE_PEEKDATA, child, addr + read);
		if (errno != 0)
		{
			val[read] = 0;
			break;
		}
		memcpy(val + read, &tmp, sizeof(tmp));
		if (memchr(&tmp, 0, sizeof(tmp)) != NULL)
			break;
		read += sizeof(tmp);
	}
	return val;
}

long ft_get_arg(pid_t child, int i)
{
	if (i == 0)
		return (get_reg(child, ebx));
	if (i == 1)
		return (get_reg(child, ecx));
	if (i == 2)
		return (get_reg(child, edx));
	if (i == 3)
		return (get_reg(child, esi));
	if (i == 4)
		return (get_reg(child, edi));
	if (i == 5)
		return (get_reg(child, ebp));
}

void ft_aff_args(pid_t child, int num, int *ret, int *len)
{
	int nargs = SYSCALL_MAXARGS, type, i, esc = 32;
	long arg, size;
	char *str;
	struct syscall_entry *se = NULL;

	if (num < MAX_SYSCALL_NUM && syscalls[num].name)
	{
		se = &syscalls[num];
		nargs = se->nargs;
	}
	for (i = 0; i < nargs; i++)
	{
		arg = ft_get_arg(child, i);
		type = se ? se->args[i] : ARG_PTR;
		if (type == ARG_INT)
		{
			*len += dprintf(2, "%d", (int)arg);
			if (strcmp(se->name, "exit_group") == 0)
				*ret = arg;
		}
		else if (type == ARG_STR)
		{
			str = read_string(child, arg);
			size = strlen(str);
			size = size > 32 ? 32 : size;
			ft_escape_chars(&str, &size, &esc);
			if (str[0] == '\0')
				*len += dprintf(2, "\"...\"");
			else
			{
				*len += dprintf(2, "\"%.*s\"", esc, str);
				if (size > 32)
					*len += dprintf(2, "...");
			}
			free(str);
		}
		else
		{
			if (arg == 0)
				*len += dprintf(2, "NULL");
			else
				*len += dprintf(2, "%#lx", arg);
		}
		if (i != nargs - 1)
			*len += dprintf(2, ", ");
	}
}

const char *sys_name(int num)
{
	struct syscall_entry *se;
	static char buf[128];

	if (num < MAX_SYSCALL_NUM)
	{
		se = &syscalls[num];
		if (se->name)
			return se->name;
	}
	snprintf(buf, sizeof(buf), "sys_%d", num);
	return buf;
}

enum argtype print_syscall(pid_t child, int *ret, int *len)
{
	int num;
	const char *name;

	num = get_reg(child, orig_eax);
	name = sys_name(num);
	*len = dprintf(2, "%s(", name);
	ft_aff_args(child, num, ret, len);
	return syscalls[num].ret;
}

void do_trace(pid_t child)
{
	int status, done = 0, ret = 0, len = 0;
	long syscall, nb_arg, retval = 0;
	enum argtype at;

	waitpid(child, &status, 0);
	ptrace(PTRACE_SETOPTIONS, child, 0, PTRACE_O_TRACESYSGOOD);
	while (42)
	{
		if (wait_for_syscall(child)) break;
		at = print_syscall(child, &ret, &len);
		if (wait_for_syscall(child)) done = 1;
		len = 42 - len;
		len = len <= 0 ? 1 : len;
		dprintf(2, ")%*s= ", len, " ");
		retval = get_reg(child, eax);
		if (errno == 0)
		{
			if (at == ARG_INT)
			{
				if (retval < 0)
				{
					dprintf(2, "-1 ");
					dprintf(2, "(%s)\n", strerror(-retval));
				}
				else
					dprintf(2, "%ld\n", retval);
			}
			else
				dprintf(2, "%#lx\n", retval);
		}
		else
			dprintf(2, "?\n");
		if (done == 1) break;
	}
	dprintf(2, "+++ exited with %d +++\n", ret);
}
